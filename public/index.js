// element constants
const nameInput = document.getElementById('nameInput');
const setSelect = document.getElementById('setSelect');
const typeSelect = document.getElementById('typeSelect');
const classSelect = document.getElementById('classSelect');
const mobileTable = document.getElementById('mobileTable');
const desktopGrid = document.getElementById('desktopGrid');
const apiInput = document.getElementById('apiInput');
const selectedLabel = document.getElementById('selectedLabel');
const manaLabel = document.getElementById('manaLabel');
const manaInput = document.getElementById('manaInput');
const selectedDiv = document.getElementById('selectedDiv');
const saveButton = document.getElementById('saveButton');
const loadButton = document.getElementById('loadButton');
// other variables
var allCards = null;
var filteredCards = [];
var selectedCards = [];
var sets = [];
var setsObj = {};
var classes = {};

// event binding
manaInput.onchange = onManaCostChanged;
setSelect.onchange = onFilterChanged;
typeSelect.onchange = onFilterChanged;
classSelect.onchange = onFilterChanged;
nameInput.onchange = onFilterChanged;
document.getElementsByName('flexRadioDefault').forEach(rButton => {
    rButton.onclick = onRadioButtonChanged;
});
document.getElementById('clearButton').onclick = onClearButtonChanged;
document.getElementById('saveApiButton').onclick = () => {
    localStorage.setItem('api', document.getElementById('apiInput').value);
};
saveButton.onclick = loadJson;

// pre-load tasks
loadSlugs();
preLoadCards();
loadApiKey();

// functions

async function preLoadCards() {
    await loadCards();
    filterCards('', '', '', '');
}

function loadApiKey() {
    let api = localStorage.getItem('api');
    if (api == null) api = '';
    document.getElementById('apiInput').value = api;
}

async function loadSlugs() {
    let response = await fetch('data/metadata.json');
    let metadata = await response.json();
    console.log(metadata);
    metadata['sets'].forEach(element => {
        setSelect.innerHTML += createOption(element['name'], element['slug']);
        sets.push(element['slug']);
        setsObj[element.id] = element.name;
    });
    metadata['classes'].forEach(element => {
        classSelect.innerHTML += createOption(element['name'], element['id']);
        classes[element.id] = element.name;
    });
}

async function loadCards() {
    let response = await fetch('data/cards.json');
    let cards = await response.json();
    allCards = cards;
}

function filterCards(name = nameInput.value, set = setSelect.value,
    type = typeSelect.value, classVar = classSelect.value) {
    let selectedSet = [];
    if (set == '') {
        sets.forEach(set => {
            allCards[set].forEach(card => {
                selectedSet.push(card);
            });
        });
    }
    else { selectedSet = allCards[set]; }
    let filtered = [];
    selectedSet.forEach(card => {
        if (filterByElement(card, type, 'cardTypeId') && filterByElement(card, classVar, 'classId')
            && filterMana(card['manaCost']) && (name == '' || card['name'].toUpperCase().includes(name.toUpperCase()))) {
            filtered.push(card);
        }
    });
    filterMana(0);
    drawGrid(filtered);
    fillTable(filtered);
    filteredCards = filtered;
}

async function drawGrid(cards) {
    let newInnerHtml = "";
    cards.forEach(card => {
        newInnerHtml +=
            `<map name="${card['id']}">
                <area shape="rect" coords="0,0,193,266" href="javascript:void(0);" onclick="onGridClicked(${card['id']})">
                </map>
                <img src="images/${card['id']}.png" class="desktop-image" usemap="#${card['id']}"/>
                `; //<img src="images/${card['id']}.png" class="desktop-image" usemap="#${card['id']}"/>
    });
    desktopGrid.innerHTML = newInnerHtml;
}

async function fillTable(cards) {
    let newInnerHtml = "";
    cards.forEach(card => {
        newInnerHtml +=
            `
            <tr id="mobile${card.id}">
                <td>${card.name}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn" style="background-color: #9436c9;" data-bs-toggle="modal" 
                        data-bs-target="#exampleModal" onclick="onInfoButtonClicked(${card.id}, 'filtered')">
                            <i class="bi bi-info-square"></i>
                        </button> 
                        <button type="button" class="btn btn-success" onclick="onAddButtonClicked(${card.id})">
                            <i class="bi bi-plus-square"></i>
                        </button>
                    </td>
                </tr>
            `;
    });
    mobileTable.innerHTML = newInnerHtml;
}


function createOption(name, value) {
    return `<option value='${value}'>${name}</option>`;
}

function onManaCostChanged() {
    manaLabel.innerHTML = `Mana cost: ${manaInput.value}`;
    if (document.querySelector('input[name="flexRadioDefault"]:checked').id != 'doNotUse')
        filterCards();
};

function onFilterChanged() {
    filterCards();
}

function filterByElement(card, element, elementName) {
    if (element == "" || card[elementName] == element) return true;
    else return false;
}

function onRadioButtonChanged() {
    filterCards();
}

function onClearButtonChanged() {
    nameInput.value = '';
    onFilterChanged();
}

function onRemoveButtonClicked(id) {
    for (let i = 0; i < selectedCards.length; i++) {
        if (selectedCards[i].id == id) {
            RemoveCard(i);
            return;
        }
    }
}

function onAddButtonClicked(id) {
    for (let i = 0; i < filteredCards.length; i++) {
        if (filteredCards[i].id == id) {
            addCard(filteredCards[i]);
            return;
        }
    }
}

function onInfoButtonClicked(id, src) {
    if (src == 'selected') {
        for (let i = 0; i < selectedCards.length; i++) {
            if (selectedCards[i].id == id) {
                setModalContent(selectedCards[i]);
                return;
            }
        }
    }
    else if (src == 'filtered') {
        for (let i = 0; i < filteredCards.length; i++) {
            if (filteredCards[i].id == id) {
                setModalContent(filteredCards[i]);
                return;
            }
        }
    }
}

function onGridClicked(id) {
    for (let i = 0; i < filteredCards.length; i++) {
        if (filteredCards[i].id == id) {
            addCard(filteredCards[i]);
            return;
        }
    }
}

function addCard(card) {
    let count = selectedCards.filter(e => e['id'] == card['id']).length;
    if (count >= 2 || selectedCards.length >= 30) return;
    selectedCards.push(card);
    if (count == 1) {
        document.getElementById(`count${card['id']}`).innerHTML = '2';
        selectedLabel.innerHTML = 'Selected ' + selectedCards.length + '/30 cards';
        return;
    }
    let newRow =
        `
            <tr id="${card.id}">
                <td>${card.name}</td>
                <td style="text-align: center;" id="count${card.id}">1</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn" style="background-color: #9436c9;" data-bs-toggle="modal" 
                        data-bs-target="#exampleModal" onclick="onInfoButtonClicked(${card.id}, 'selected')">
                            <i class="bi bi-info-square"></i>
                        </button>
                        <button type="button" class="btn btn-danger" onclick="onRemoveButtonClicked(${card.id})">
                            <i class="bi bi-trash"></i>
                        </button>
                    </td>
                </tr>
            `;
    selectedDiv.innerHTML += newRow;
    selectedLabel.innerHTML = 'Selected ' + selectedCards.length + '/30 cards';
}

function filterMana(manaCost) {
    let selectedMana = manaInput.value;
    let selectedMode = document.querySelector('input[name="flexRadioDefault"]:checked').id;
    switch (selectedMode) {
        case 'moreThan':
            return manaCost > selectedMana;
        case 'lessThan':
            return manaCost < selectedMana;
        case 'equals':
            return manaCost == selectedMana;
        default:
            return true;
    }
}

function RemoveCard(i) {
    let count = selectedCards.filter(e => e['id'] == selectedCards[i]['id']).length;
    if (count == 2) {
        document.getElementById('count' + String(selectedCards[i].id)).innerHTML = '1';
    }
    else {
        document.getElementById(selectedCards[i]['id']).remove();
    }
    selectedCards.splice(i, 1);
    selectedLabel.innerHTML = 'Selected ' + selectedCards.length + '/30 cards';
}

function setModalContent(card) {
    let innerHTML =
        `
            <div class="row">
            <div class="col-md-5 col-sm-12 align-items-center d-flex justify-content-center" style="align-content: center;">
            <img src="images/${card['id']}.png" class="desktop-image""/>
            </div>
            <div class="col-md-7 col-sm-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Class:</td>
                            <td>${classes[card.classId]}</td>
                        </tr>
                        <tr>
                            <td>Set:</td>
                            <td>${setsObj[card.cardSetId]}</td>
                        </tr>
                        <tr>
                            <td>Artist name:</td>
                            <td>${card.artistName}</td>
                        </tr>
                        <tr>
                            <td>Collectible:</td>
                            <td>${card.collectible == 1 ? 'Yes' : 'No'}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
            `; //<img src="images/${card['id']}.png" class="desktop-image" usemap="#${card['id']}"/>
    document.getElementById('modalBody').innerHTML = innerHTML;
}

function loadJson() {
    const jsonData = JSON.stringify(selectedCards);
    const blob = new Blob([jsonData], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const downloadLink = document.createElement('a');
    downloadLink.href = url;
    downloadLink.download = 'data.json';
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}