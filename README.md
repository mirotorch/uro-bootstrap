# Hearthstone Deck Master

- [Hearthstone Deck Master](#hearthstone-deck-master)
  - [Overview](#overview)
  - [Features](#features)
  - [Installation](#installation)
  - [Data Files](#data-files)
  - [Project Structure](#project-structure)

## Overview

"Hearthstone Deck Master" is a web application designed to help users create and manage their Hearthstone card decks.
It provides an interface to browse and filter Hearthstone cards based on various criteria such as mana cost,
card type, and class.

## Features

- **Card Browsing**: Users can browse Hearthstone cards with filters for mana cost, card set, type, and class.
- **Deck Management**: Build and manage custom decks. Save and load decks using JSON files.
- **Blizzard API Integration**: Users can enter their Blizzard API key to fetch real-time data.

## Installation

To set up the project locally, follow these steps:

1. Clone the repository to your local machine.
2. Ensure you have Node.js installed.
3. Navigate to the project directory and run `npm install` to install dependencies.
4. Start the server with `npm start`.
5. Access the application through `localhost:5500/public/index.html`.

## Data Files

The application uses JSON files to store card data (`cards.json`), card classes (`classes.json`), and other metadata.

## Project Structure

- `public/`: Contains the HTML, CSS, and JavaScript files for the UI.
- `data/`: Stores JSON files used for card and application data.

For more information on how to use the application or to report issues, please visit the project's homepage or issues section on GitHub.
